import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
titulo = "App Motos";
 
cardS = [
  { 
    titulo: "Suzuki moto ",
    subtitulo: "Elaborada com alta tecnologia as motos suzuki gsx-1300 tem potência alta, força e lhe garante o melhor conforto",
    conteudo: "Este é um exemplo de como ficarão os cards do meu app",
    foto: "https://images.noticiasautomotivas.com.br/img/f/fotos-de-motos-7.jpg"
  },

  {
    titulo: "Ducati Superbike 1299 Panigale",
    subtitulo: "A Ducati 1299 Panigale é uma moto esportiva Ducati de 1.285 cc, apresentada no Milan Motorcycle Show 2014 e produzida desde 2015 como sucessora do 1.198 cc 1199. A motocicleta recebeu o nome da pequena cidade industrial de Borgo Panigale. A distância entre eixos de 1299 permanece a mesma em 1437 ",
    conteudo: "Este é o segundo exemplo de card no meu aplicativo",
    foto: "https://s1.1zoom.me/big0/489/388088-blackangel.jpg"
  },
  {
    
    titulo: "Triumph Bonneville T120",
    subtitulo: "O Triumph Bonneville T120 é uma motocicleta fabricada originalmente pela Triumph Engineering de 1959 a 1975. Foi o primeiro modelo da série Bonneville, que foi continuado pela Triumph Motorcycles Ltd. O T120 foi descontinuado em favor do maior T140 de 750 cc no início 1970s.",
    conteudo: "Este é o terceiro exemplo de card no meu aplicativo",
    foto :"https://www.motonline.com.br/storage/guides/suzuki/intruder-vs-1400-glp.jpg"
  },
  {
    
    titulo: "Ducati Panigale V4 Speciale",
    subtitulo: "Para ajudar o piloto a dominar essa fera, a Ducati colocou itens de segurança ativa e controle da dinâmica do veículo. Tem controle de derrapagem Ducati Slide Control, atuando no controle de tração e freios ABS. Esse sistema consegue determinar o quanto as rodas traseiras podem derrapar para ajudar nas curvas. Ainda tem ABS para curvas e quick-shift para trocas rápidas de marcha.",
    conteudo: "Exemplo 4",
    foto :"https://http2.mlstatic.com/D_NQ_NP_973618-MLB31858989456_082019-O.jpg"
  }
];
 
  constructor() {}

}
